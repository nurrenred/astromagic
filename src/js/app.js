global.$ = require('jquery');
global.jQuery = require('jquery');
require ('bootstrap');


/**
 * Require blocks
 */

// require('./_blocks')();
var owlCarousel = require('owl.carousel');


var owl = $("#teamSlider");
owl.owlCarousel({
    nav: true,
    items: 3,
    loop: true,
    autoplay: false,
    autoplayTimeout: 15000,
    dots: false,
    lazyLoad:true,
    navText : ["<i class='fa fa-chevron-right'></i>","<i class='fa fa-chevron-left'></i>"],
    responsive:{
            0:{
                    items:1,
                    nav: true
            },
            768:{
                    items:2,
                    nav: true
            },
            920:{
                    items:3,
                    nav: true
            },
            1280:{
                    items:3,
                    nav: true
            }
    }
});



var owl = $("#reviewsSlider");
owl.owlCarousel({
    nav: true,
    items: 1,
    loop: true,
    autoplay: false,
    autoplayTimeout: 15000,
    dots: false,
    lazyLoad:true,
    navText : ["<i class='fa fa-chevron-right'></i>","<i class='fa fa-chevron-left'></i>"],
//     responsive:{
//             0:{
//                     items:1,
//                     nav: true
//             },
//             1280:{
//                     items:1,
//                     nav: true
//             }
//     }
});


$(".reviewsPageSlider").owlCarousel({
    nav: true,
    items: 1,
    loop: true,
    autoplay: false,
    autoplayTimeout: 15000,
    dots: false,
    lazyLoad:true,
    navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
});

// Sonnik autocomplite

var Sonnik = require('./sonnik.js');

let sonnikEl = document.getElementById("sonnik");
if(sonnikEl)
	new Sonnik(document.getElementById("sonnik"));

var quickАortunetelling = require('./quickАortunetelling.js');
new quickАortunetelling();

document.addEventListener("DOMContentLoaded", function(){
	document.querySelector('.main-header__image').classList.add('start');

	setTimeout(function(){
		document.querySelector('.main-header__image').classList.remove('start');
	}, 4000);
});


// Horoscope
var Horoscope = require('./horoscope.js');

var horoscopeEl = document.getElementById("horoscope");
new Horoscope(horoscopeEl);


