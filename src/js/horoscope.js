function Horoscope(el){
	
	var th = this;

	this.currentHoroscope = {};

	this.resourceId;
	
	this.horoscopeDate = [
		// {
		// 	'date' : '14.02',
		// 	'title' : 'Овен',
		// 	'text': 'Text 1'
		// },
		// {
		// 	'date' : '15.02',
		// 	'title' : 'Овен',
		// 	'text': 'Text 2'
		// },
		// {
		// 	'date' : '16.02',
		// 	'title' : 'Овен',
		// 	'text': 'Text 3'
		// }
	];

	/**
	 * Получение данных
	 */
	this.getData = function(){
		th.getId();

		$.get('horoscopApi.json?id=' + th.resourceId, function(data){
			th.horoscopeDate = data;

			console.log(data.responseText);
		
			th.dateSelect();

			th.horoscopeDefault();
		}).fail(function(e) {
			console.log(e);
		});
	}

	/**
	 * Id ресурса
	 */
	this.getId = function(){
		th.resourceId = el.getAttribute('data-id');		
	}

	/**
	 * Событие выбора даты
	 */
	this.dateSelect = function(){
		var dataLinks = el.querySelectorAll('.horoscope__date-link');
		var selectDate = '';

		for(var i = 0; i < dataLinks.length; i++){
			dataLinks[i].addEventListener('click', function(e){
				e.preventDefault();

				// Смена активного пункта меню
				for(var dataLinksItem = 0; dataLinksItem < dataLinks.length; dataLinksItem++){
					dataLinks[dataLinksItem].parentElement.classList.remove('horoscope__date-item--active');
				}
				this.parentElement.classList.add('horoscope__date-item--active');

				selectDate = this.getAttribute('data-date');

				th.currentHoroscope = th.getHoroscopeByDate(selectDate);

				th.horoscopeRender();

				// Меняем название дня
				el.querySelector('.dayName').innerHTML = this.innerHTML.toLowerCase();
			});
		}
	}

	/**
	 * Получаем гороскоп по дате
	 */
	this.getHoroscopeByDate = function(date){
		for(var i = 0; i < th.horoscopeDate.length; i++){
			if( th.horoscopeDate[i].date ==  date){
				return th.horoscopeDate[i];
			}
		}
	}

	
	/**
	 * Значение по умолчанию
	 */
	this.horoscopeDefault = function(){
		
		var date = el.querySelector('.horoscope__date-item--active .horoscope__date-link').getAttribute('data-date');

		th.currentHoroscope = th.getHoroscopeByDate(date);

		th.horoscopeRender();
	}

	/**
	 * Рендер гороскопа
	 */
	this.horoscopeRender = function(){
		if(!!th.currentHoroscope){
			el.querySelector('.horoscope__content-text').innerHTML = th.currentHoroscope.text;
		}
		else{
			el.querySelector('.horoscope__content-text').innerHTML = 'Гороскоп не найден на этот день.';
		}
	}

	/**
	 * Инициализация
	 */
	this.init = function(){
		if(!el) return;
console.log('init');
		th.getData();
	}
	

	this.init();
}

module.exports = Horoscope;