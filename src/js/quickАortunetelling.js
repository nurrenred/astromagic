function QuickАortunetelling(){

	var th = this;
	var currentMess = '';
	
	this.data = quickАortunetellingData;

	this.btnClick = function(){
		document.querySelector('.main-header__fortunetelling-btn').addEventListener('click', function(){

			// Получаем случайный ключ массива
			var rand = Math.floor(Math.random() * th.data.length);
			th.currentMess = th.data[rand];

			th.showMess();
		});		
	}

	this.closeClick = function(){
		document.querySelector('.main-header__fortunetelling-close').addEventListener('click', function(){
			th.closeMess();
		});
	}

	th.showMess = function(){
		document.querySelector('.main-header__fortunetelling-text').classList.remove('show');
		document.querySelector('.main-header__image').classList.add('animate');
		document.querySelector('.main-header__fortunetelling-btn').setAttribute('disabled', 'disabled');
		setTimeout(function(){
			document.querySelector('.main-header__image').classList.remove('animate');
			document.querySelector('.main-header__fortunetelling-text-wrap').innerHTML = th.currentMess;
			document.querySelector('.main-header__fortunetelling-text').classList.add('show');
			document.querySelector('.main-header__fortunetelling-btn').removeAttribute('disabled');
		}, 4000);
	}

	th.closeMess = function(){
		document.querySelector('.main-header__fortunetelling-text').classList.remove('show');
		document.querySelector('.main-header__fortunetelling-text-wrap').innerHTML = '';
	}



	this.init = function(){
		th.btnClick();
		th.closeClick();
	}

	this.init();
}


module.exports = QuickАortunetelling;