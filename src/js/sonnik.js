var autocomplete = require('../blocks/autocomplete/autocomplete');

function Sonnik(el){
	var th = this;

	this.data = [];

	this.getData = function(){		
		$.get('sonnikapi.json', function(data){

			th.data = data;

			th.autocomplete();
			th.formSubmit();			
		})
		.fail(function(e) {
			console.log(e);
		});	



		// th.data = [
		// 	{
		// 			"id": "68",
		// 			"title": "Дом",
		// 			"link": "dom.html"
		// 	},
		// 	{
		// 			"id": "67",
		// 			"title": "Насорог",
		// 			"link": "nasorog.html"
		// 	},
		// 	{
		// 			"id": "65",
		// 			"title": "Свадьба",
		// 			"link": "svadba.html"
		// 	},
		// 	{
		// 			"id": "66",
		// 			"title": "Рыба",
		// 			"link": "ryiba.html"
		// 	},
		// 	{
		// 			"id": "62",
		// 			"title": "Замок",
		// 			"link": "zamok.html"
		// 	},
		// 	{
		// 			"id": "63",
		// 			"title": "Собака",
		// 			"link": "sobaka.html"
		// 	},
		// 	{
		// 			"id": "61",
		// 			"title": "Слон",
		// 			"link": "slon.html"
		// 	}];
	}

	this.autocomplete = function(){
		var autocompleteData = [];

		for(var i = 0; i < th.data.length; i++){			
			autocompleteData.push(th.data[i].title);
		}

		autocomplete(el.querySelector(".sonnik__input"), autocompleteData);
		
	}

	this.formSubmit = function(){
		el.querySelector('.sonnik__form').addEventListener('submit', function(e){
			e.preventDefault();

			var dataId = el.querySelector(".sonnik__input").getAttribute('data-id');
			var link;
			
			if(dataId === null){
				console.log('Такого нет');
			}
			else{
				link = th.data[dataId]['link'];
				window.location = link;
			}
			
		});
	}

	this.letterRender = function(){
		var lettersString = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЭЮЯ';
		var htmlString = '';

		for(var i = 0; i < lettersString.length; i++){
			htmlString += '<button data-letter="'+ lettersString[i].toLowerCase() +'" class="sonnik__btn sonnik__letter-btn">'+ lettersString[i] +'</button>';		
		}

		el.querySelector('.sonnik__letter-btn-list').innerHTML = htmlString;
	}

	this.letterBtnEvent = function(){
		var btnList = el.querySelectorAll('.sonnik__letter-btn');

		for(var i = 0; i < btnList.length; i++){
			btnList[i].addEventListener('click', function(e){
				e.preventDefault();
				
				for(var y = 0; y < btnList.length; y++){
					btnList[y].classList.remove('sonnik__btn--active');
				}
				this.classList.add('sonnik__btn--active');

				var letter = this.getAttribute('data-letter');

				var letterResultIds = th.getTitleByFirstLetter(letter);

				th.letterResultRender(letterResultIds);
			});
		}
	}

	this.letterResultRender = function(arr){
		var html = '';
		
		for(var i = 0; i < arr.length; i++){
			html += '<li class="sonnik__letter-title"><a class="sonnik__letter-title-link" href="'+ th.data[arr[i]].link +'">'+ th.data[arr[i]].title +'</a></li>';
		}

		el.querySelector('.sonnik__letter-title-list').innerHTML = html;
	}

	this.getTitleByFirstLetter = function(letter){
		var result = [];
		for(var i = 0; i < th.data.length; i++){
			if( th.data[i]['title'][0].toUpperCase() == letter.toUpperCase() ){
				result.push(i);
			}
		}

		return result;
	}

	this.init = function(){
		th.getData();

		th.letterRender();

		th.letterBtnEvent();
	}

	this.init();
}


module.exports = Sonnik;